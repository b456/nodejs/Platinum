const { validation } = require('../middleware/validation')
const { Users } = require('../models')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const express = require ('express')

const app = require('http');
const io = require('socket.io')(app);


//register
const register = async (req,res) => {
    try {
        const{firstname, lastname, email, phone, gender, addres, password} = req.body
        //form validation
        const validate = validation(req.body)
        if(validate.length){
            return res.status(400).json(validate)
        }

        //bcrypt
        const salt = await bcrypt.genSaltSync(10)
        const hashPassword = bcrypt.hashSync(password, salt)
        const users = new Users({
            firstname: firstname,
            lastname:lastname,
            email:email,
            phone: phone,
            gender: gender,
            addres: addres,
            password: hashPassword
        })

        const saveUser = await users.save(req.body)
        return res.status(200).json({
            message: 'succses',
            data: saveUser
        })
    } catch (error) {
        console.log(error)
        res.json({
            message: 'catch eror'
        })
    }
}

//login
const login = async (req,res) =>{
    try {
        const email = req.body.email
        const password = req.body.password

        const checkUser = await Users.findOne({where:{email:email}})
        if(!checkUser) res.status(400).send('email not found')

        const resultLogin = bcrypt.compareSync(password, checkUser.password)
        if(!resultLogin) res.status(400).send('something was wrong')

        //token
        const token = jwt.sign({_email: checkUser.email}, process.env.TOKEN_RAHASIA)
        res.header('auth-token', token).send('succses login')
    } catch (error) {
        return res.send('eror')
    }
}

let messages = [];





const getMessages = (req, res) => {
    res.json(messages);
};
io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});
const sendMessages = (req, res) => {
    const { sender, recipient, message } = req.body;
    console.log(req.body);
    const newMessage = {
        sender,
        recipient,
        message,
        timestamp: Date.now()
    };
    messages.push(newMessage);
    io.emit('new message', newMessage);
    res.json(newMessage);
};


module.exports = {
    register,
    login,
    getMessages,
    sendMessages
}