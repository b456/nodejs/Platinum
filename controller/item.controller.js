const multer = require('multer')
const cloudinary = require('cloudinary')
const fs = require('fs')
const {Items} = require('../models')

const storage = multer.diskStorage({
  destination: function(req,file,callback){
      callback(null, './uploads')
  },
  filename: function(req, file, callback){
      callback(null, file.originalname)
  }   
})
const upload = multer ({storage: storage})

cloudinary.config({
  cloud_name: 'dzdayzkw0',
  api_key: '458172166673443',
  api_secret: 'aW425k5ALEiyfQRoT7yL0mxiuZU' 
})

async function uploadCloudinary(filePath){
  let result;
  try {
      result = await cloudinary.uploader.upload(filePath, {
          use_filename: true}) 
      fs.unlinkSync(filePath)
      return result.url
  } catch(err){
      fs.unlinkSync(filePath)
      return null
  }
}

const uploads = upload.single('avatar')

// upload
const uploadFile = async (req,res) =>{
  
  const url = await uploadCloudinary(req.file.path)
  const {name, qty, harga, warna } = req.body

  if(url){
    const item = await Items.create({
      name, warna, qty, harga, url
    })
  return res.json({
      message: 'upload berhasil',
      item:item,
    })
  }else{
    return res.json({
      message: 'upload gagal',
    })
  }
}

// get data
const getAll = async(req, res) => {
  try{
    Items.findAll()
    .then(items => {
        return res.status(200).json(items)
      })
  }catch(err){
    return req.status(400).json({
      message: err
    })
  }
}

// get one
const getOne = async(req, res) => {
  try{
    const item = await Items.findOne({
      where: {id: req.body.id}
    }).then(items => {
      return res.status(200).json(items)
    })
  }catch(err){
    return req.status(400).json({
      message: err
    })
  }
}

// update
const update = async(req, res) =>{
  try{
    const url = await uploadCloudinary(req.file.path)
  const {name, qty, harga, warna} = req.body
  
    const item = await Items.update({
      name, warna, qty, harga, url
    }, {
      where: {id: req.params.id}
    })
      // console.log(item);
      return res.status(200).json({
        message: 'update berhasil',
        item: item
      })
}catch(err){
  return res.status(400).json({
    message: 'update gagal',
  })
  }
}



//delete
 const deleteItem = async(req, res) =>{
  try{
    const item = await Items.destroy({
      where: {id: req.body.id}
    })
    return res.status(200).json({
      message: 'delete berhasil',
      item: item
    })
  }catch(err){
    return res.status(400).json({
      message: err
    })
  }
}




module.exports = {
  uploads,
  uploadFile,
  getAll,
  update,
  deleteItem,
  getOne
}