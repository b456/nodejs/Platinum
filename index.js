const express = require('express')
const app = express()
const router = require('./routes/router')
const itemRoutes = require('./routes/item.routes')
// const up = require('./controller/item.controller')

app.use(express.urlencoded({extended:false}))
app.use(express.json())

app.use('/', router)
app.use('/', itemRoutes)
// app.post('/profile', up.uploads, up.uploadFile, up.uploadFiles)


app.listen(8000, ()=>{
    console.log('server runing')
})

