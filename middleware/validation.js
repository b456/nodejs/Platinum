const Validator = require('fastest-validator')
const v = new Validator(
    {
    useNewCustomCheckerFunction: true, // using new version
    messages: {
        // Register our new error message text
        phoneNumber: "The phone number must be started with '+62'!"
    }
}
)


const validation = data => {
    const schema = {
        firstname: {type: "string", min: 1},
        lastname: {type: "string", min: 1},
        email: {type: "email"},
        phone: { type: "string", min: 13, max: 14, custom: (v, errors) => {
            if (!v.startsWith("+62")) errors.push({ type: "phoneNumber" })
            return v.replace(/[^\d+]/g, ""); // Sanitize: remove all special chars except numbers
        }},
        gender: {type: "enum", values: ["male", "female"]},
        addres: {type: "string"},
        password: {type: "string",min: 3 }
    }
    return v.validate(data, schema)
} 

module.exports.validation = validation
