const express = require('express')
const router = express.Router()
const auth = require('../controller/auth')
const verifyToken = require('../middleware/verify-token')
const testController = require('../controller/test-controller')

router.post('/register', auth.register)
router.post('/login',auth.login)

//messages
router.get('/messages', auth.getMessages)
router.post('/messages', auth.sendMessages)

//test
router.get('/dashboard/:id',verifyToken,testController.dashboard)


module.exports = router