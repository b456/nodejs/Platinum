const ctrl = require('../controller/item.controller')
const router = require('express').Router()

router.post('/upload', ctrl.uploads, ctrl.uploadFile)
router.put('/updateItem/:id', ctrl.update)
router.get('/getAll', ctrl.getAll)
router.get('/getOne/:id', ctrl.getOne)
router.delete('/delete/:id', ctrl.deleteItem)



module.exports = router